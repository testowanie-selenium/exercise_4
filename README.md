# Zadanie domowe nr 4

## Testowanie różnego rodzaju kontrolek

### Cel testów

Mamy do dyspozycji stronę (http://the-internet.herokuapp.com) zbudowaną na potrzeby nauki Selenium. Jest tam umieszczony cały zbiór różnego rodzaju elementow występujących na stronach www. 


Twoim zadaniem jest utworzyć testy do 3 wybranych elementów. 
* Zastosuj wzorzec PageObjectModel 
* Tam, gdzie się da, zastosuj lokalizator CSS

https://www.guru99.com/locators-in-selenium-ide.html#4


### Wskazówki

Zwróć uwagę na klasę **CheckboxesPage**. *Dziedziczy* ona po głównej klasie **HerokuMainPage** i stamtąd pobiera główny adres serwisu. Implementując pozostałe klasy dla PageObjects, możesz zastosować ten sam schemat.
Również elementy na tej stronie zdefiniowałem jako właściwości (pola) klasy. Dzięki temu nie musimy w poszczególnych metodach powtarzać lokalizatorów.

```java
public class CheckboxesPage extends HerokuMainPage {

    private By checkBox1 = By.cssSelector("#checkboxes > input[type=checkbox]:nth-child(1)");
    private By checkBox2 = By.cssSelector("#checkboxes > input[type=checkbox]:nth-child(3)");

    public CheckboxesPage(WebDriver driver) {
        super(driver);
    }

...

```




