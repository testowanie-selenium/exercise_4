package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HerokuMainPage {

    public static final String BASE_URL = "http://the-internet.herokuapp.com";

    public By checkBox1 = By.cssSelector("#checkboxes > input[type=checkbox]:nth-child(1)");
    public By checkBox2 = By.cssSelector("#checkboxes > input[type=checkbox]:nth-child(3)");

    public String username = "tomsmith";
    public String password = "SuperSecretPassword!";
    public String wrongUsername = "TomSmithh";
    public String wrongPassword = "SuperSecredPassword!!!";
    public By formUsername = By.cssSelector("#username");
    public By formPassword = By.cssSelector("#password");
    public By submitButton = By.cssSelector("#login > button > i");
    public By logoutButton = By.cssSelector("#content > div > a > i");
    public By info = By.xpath("//*[@id=\"flash\"]");

    protected WebDriver driver;

    public HerokuMainPage(WebDriver driver) {
        this.driver = driver;
    }

}
