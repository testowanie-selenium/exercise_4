package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends HerokuMainPage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        driver.get(BASE_URL + "/login");
    }

    public void fillFormUsername() {
        WebElement form =  driver.findElement(formUsername);
        form.sendKeys(username);
    }

    public void fillFormPassword() {
        WebElement form =  driver.findElement(formPassword);
        form.sendKeys(password);
    }

    public void submitButton() {
        driver.findElement(submitButton).click();
    }

    public void logoutButton() {
        driver.findElement(logoutButton).click();
    }

    public String successInfo() {
        System.out.println(driver.findElement(info).getText().trim());
        return driver.findElement(info).getText().trim();

    }

    public void fillFormWithWrongUsername() {
        WebElement form = driver.findElement(formUsername);
        form.sendKeys(wrongUsername);
    }

    public void fillFormWithWrongPassword() {
        WebElement form = driver.findElement(formPassword);
        form.sendKeys(wrongPassword);
    }

    public String errorInfo() {
//        WebElement element = driver.findElement(info);
//        System.out.println(driver.findElement(info).getText().trim());
        return driver.findElement(info).getText().trim();

    }
}
