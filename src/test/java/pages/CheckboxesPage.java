package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CheckboxesPage extends HerokuMainPage {

    public CheckboxesPage(WebDriver driver) {
        super(driver);
    }

    public void goToPage() {
        driver.get(BASE_URL + "/checkboxes");
    }

    public void setSelectionCheckbox1(boolean selected) {
        WebElement checkbox =  driver.findElement(checkBox1);
        setSelected(checkbox, selected);
    }

    public void setSelectionCheckbox2(boolean selected) {
        WebElement checkbox =  driver.findElement(checkBox2);
        setSelected(checkbox, selected);
    }

    public boolean getSelectedCheckbox1() {
        return driver.findElement(checkBox1).isSelected();
    }

    public boolean getSelectedCheckbox2() {
        return driver.findElement(checkBox2).isSelected();
    }

    private void setSelected(WebElement element, boolean selected) {
        if(element.isSelected() != selected) {
            element.click();
        }
    }

}
