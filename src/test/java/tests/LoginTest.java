package tests;
import helpers.WebDriverFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.openqa.selenium.*;
import pages.LoginPage;

import java.util.*;

public class LoginTest {
    private WebDriver driver;

    private LoginPage loginPage;

    @Before
    public void setUp() {
        driver = WebDriverFactory.getDriver();
        loginPage = new LoginPage(driver);
        loginPage.goToPage();
    }

    @After
  public void tearDown() {
    driver.quit();
  }


        @Test
    public void testfillForms() {
        loginPage.fillFormUsername();
        loginPage.fillFormPassword();
        loginPage.submitButton();
        Assert.assertEquals("You logged into a secure area!\n" + "×", loginPage.successInfo());
        loginPage.logoutButton();
    }

    @Test
    public void wrongUsername() {
        loginPage.fillFormWithWrongUsername();
        loginPage.fillFormPassword();
        loginPage.submitButton();
        Assert.assertEquals("Your username is invalid!\n" + "×", loginPage.errorInfo());
    }

    @Test
    public void wrongPassword() {
        loginPage.fillFormUsername();
        loginPage.fillFormWithWrongPassword();
        loginPage.submitButton();
        Assert.assertEquals("Your password is invalid!\n" + "×", loginPage.errorInfo());
    }
}

